import "./App.css";
import Home from "./components/home";
import Quiz from "./components/quiz";
import Result from "./components/result";
import { Route, Routes, Link } from "react-router-dom";

function App() {
  return (
    <>
      <button className="none">
        <Link to="/">Start</Link>
      </button>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/quiz" element={<Quiz />} />
        <Route path="/result" element={<Result />} />
      </Routes>
    </>
  );
}

export default App;
