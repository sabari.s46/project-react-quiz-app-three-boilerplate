import { Link, useLocation } from "react-router-dom";

const Result = () => {
  const location = useLocation();
  const { attempted, correct, wrong } = location.state;
  return (
    <div className="Content">
      <div className="Result_Content">
        <div className="Suggestion">You need more practice</div>
        <div className="Score">
          Your score is {Math.floor((correct / 15) * 100)}%
        </div>
        <div className="Details">
          <div className="details1 flex_spacebetween">
            <span>Total number of questions</span>
            <span className="result1">15</span>
          </div>
          <div className="details2 flex_spacebetween">
            <span>Number Of attempted questions</span>
            <span className="result2">{attempted}</span>
          </div>
          <div className="details3 flex_spacebetween">
            <span>Number of correct answers</span>
            <span className="result3">{correct}</span>
          </div>
          <div className="details4 flex_spacebetween">
            <span>Number Of wrong answers</span>
            <span className="result4">{wrong}</span>
          </div>
        </div>
      </div>
      <div className="buttons">
        <Link to="/quiz">
          <button>Play Again</button>
        </Link>
        <Link to="/">
          <button>Back to Home</button>
        </Link>
      </div>
    </div>
  );
};

export default Result;
