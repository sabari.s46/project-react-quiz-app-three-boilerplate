import { Component } from "react";
import "../App.css";
import { Link } from "react-router-dom";
export default class Home extends Component {
  render() {
    return (
      <div className="Content">
        <div className="Center">
          <div className="Heading">Quiz App</div>
          <Link to="/quiz">
            <div className="Button_cont">Play</div>
          </Link>
        </div>
      </div>
    );
  }
}
