import { useState } from "react";
import Data from "../resources/quizQuestions.json";
import "../App.css";
import { Link } from "react-router-dom";

const Quiz = () => {
  const [state, setState] = useState({
    questions: Data,
    currentIndex: 0,
    score: 0,
    attempted: 0,
  });

  const handleNextQuestion = () => {
    const { currentIndex, questions } = state;
    if (currentIndex < questions.length - 1) {
      setState((prevState) => ({
        ...prevState,
        currentIndex: prevState.currentIndex + 1,
      }));
    }
  };

  const handlePreviousQuestion = () => {
    const { currentIndex } = state;
    if (currentIndex > 0) {
      setState((prevState) => ({
        ...prevState,
        currentIndex: prevState.currentIndex - 1,
      }));
    }
  };

  const checkingAnswer = (answer, option) => {
    if (answer === option) {
      alert("Correct option");
      setState((prevState) => ({
        ...prevState,
        attempted: prevState.attempted + 1,
        score: prevState.score + 1,
      }));
    } else {
      alert("Wrong option");
      setState((prevState) => ({
        ...prevState,
        attempted: prevState.attempted + 1,
      }));
    }
  };

  const { currentIndex, questions, attempted, score } = state;
  const currentQuestion = questions[currentIndex];

  return (
    <div className="Content">
      <div className="Quiz_Content">
        <div className="QHeading flex_center">Question</div>
        <div className="Question_Count">{`${currentIndex + 1} of ${
          questions.length
        }`}</div>
        <div className="Question flex_center">{currentQuestion.question}</div>
        <div className="Options">
          {["A", "B", "C", "D"].map((option, index) => (
            <div
              key={index}
              className={`Option${index + 1}`}
              onClick={() =>
                checkingAnswer(
                  currentQuestion.answer,
                  currentQuestion[`option${option}`]
                )
              }
            >
              {currentQuestion[`option${option}`]}
            </div>
          ))}
        </div>
        <div className="Controls">
          <div className="Control1" onClick={handlePreviousQuestion}>
            Previous
          </div>
          <div className="Control2" onClick={handleNextQuestion}>
            Next
          </div>
          <div
            className="Control3"
            onClick={() => prompt("Are you sure you want to quit?")}
          >
            Quit
          </div>
          <Link
            to="/result"
            state={{
              attempted: attempted,
              correct: score,
              wrong: attempted - score,
            }}
          >
            <div className="Control4">Finish</div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Quiz;
